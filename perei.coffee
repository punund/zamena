csv = require 'csv'
fs = require 'fs'

wf = process.argv[2]
wt = process.argv[3]

unless wt
  console.log "Usage: #{process.argv[0]} #{process.argv[1]} from-file to-file"
  process.exit 1

console.log wf, wt

csv()
  .from.path __dirname+'/zamena.csv'
  .to.array (pairs) ->
    fs.readFile wf, 'utf8', (err, data) ->
      if (err) then return console.error err
      for pair in pairs
        re = new RegExp pair[0], 'gm'
        data = data.replace re, pair[1]
      fs.writeFile wt, data, 'utf8', (err) -> if (err) then return console.error err
  
  .on 'error', (error) -> console.error error.message
  .on 'exit', -> process.exit 0


